#include "defines.h"
#define NUM_PFDS 3

#include <errno.h>
#include <fcntl.h>
#include <poll.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <linux/uhid.h>

#include <linux/hidraw.h>
#include <sys/ioctl.h>

#include "uhid.h"
#include "smartboard.h"

bool expose_leds = 0;

static int print_command(unsigned char *buf) {
	int datalength = (buf[1] & 0x0f);
	int i;
	fprintf(stderr,"c(%x)",buf[2]);
	for(i=3;i<datalength+3;i++)
		fprintf(stderr,"|%x",buf[i]);
	fprintf(stderr,"\n");
	return 0;
}

/* This parses raw output reports sent by the kernel to the device. A normal
 * uhid program shouldn't do this but instead just forward the raw report.
 * However, for ducomentational purposes, we try to detect LED events here and
 * print debug messages for it. */
static void handle_output(struct uhid_event *ev)
{
	/* LED messages are adverised via OUTPUT reports; ignore the rest */
	if (ev->u.output.rtype != UHID_OUTPUT_REPORT)
		return;
//	/* LED reports have length 2 bytes */
//	if (ev->u.output.size != 2)
//		return;
//	/* first byte is report-id which is 0x02 for LEDs in our rdesc */
//	if (ev->u.output.data[0] != 0x2)
//		return;
	/* print flags payload */
	fprintf(stderr, "(LED) output report received with flags %x\n",
		ev->u.output.data[1]);
}
static int event(int fd) {
	struct uhid_event ev;
	ssize_t ret;
	memset(&ev, 0, sizeof(ev));
	ret = read(fd, &ev, sizeof(ev));
	if (ret == 0) {
		fprintf(stderr, "Read HUP on uhid-cdev\n");
		return -EFAULT;
	} else if (ret < 0) {
		fprintf(stderr, "Cannot read uhid-cdev: %m\n");
		return -errno;
	} else if (ret != sizeof(ev)) {
		fprintf(stderr, "Invalid size read from uhid-dev: %ld != %lu\n",
			ret, sizeof(ev));
		return -EFAULT;
	}
	switch (ev.type) {
	case UHID_START:
		fprintf(stderr, "UHID_START from uhid-dev\n");
		break;
	case UHID_STOP:
		fprintf(stderr, "UHID_STOP from uhid-dev\n");
		break;
	case UHID_OPEN:
		fprintf(stderr, "UHID_OPEN from uhid-dev\n");
		break;
	case UHID_CLOSE:
		fprintf(stderr, "UHID_CLOSE from uhid-dev\n");
		break;
	case UHID_OUTPUT:
		fprintf(stderr, "UHID_OUTPUT from uhid-dev\n");
		handle_output(&ev);
		break;
	case UHID_OUTPUT_EV:
		fprintf(stderr, "UHID_OUTPUT_EV from uhid-dev\n");
		break;
	default:
		fprintf(stderr, "Invalid event from uhid-dev: %u\n", ev.type);
	}
	return 0;
}

static int smartboard(int uhid_fd, int smartboard_fd) {
	unsigned char buf[MAX_REPORT_SIZE];
	ssize_t n;
	n = read(smartboard_fd, buf, smartboard_model->report_size);
	if (n == 0) {
		fprintf(stderr, "Read HUP on smartboard\n");
		return -EFAULT;
	} else if (n < 0) {
		fprintf(stderr, "Cannot read smartboard: %m\n");
		return -errno;
	} else {
		fprintf(stderr, "%i bytes read from smartboard\n",(int)n);
	}

	if(buf[0] == REPORT_ID) { //report ID 2 is what we're after
		print_command(buf);
		process_command(uhid_fd, smartboard_fd, buf[2], buf+3);
	}

	return 0;
}
static int keyboard() {
	unsigned char buf[100];
	ssize_t n;
	n = read(0,buf,sizeof(buf));
	if(n == 0) {
		fprintf(stderr, "Read HUP on stdin\n");
		return -EFAULT;
	} else if(n < 0) {
		fprintf(stderr, "Cannot read stdin: %m\n");
		return -errno;
	} else if(buf[0] == 'q'){
		return 1;
	}
	return 0;
}
int main(int argc, char **argv) {

	int uhid_fd;
	int smartboard_fd;
	const char *uhid_path = "/dev/uhid";
	char *smartboard_path;
	struct pollfd pfds[NUM_PFDS];
	struct termios state;

	if (tcgetattr(STDIN_FILENO, &state)) {
		fprintf(stderr, "Cannot get tty state\n");
	} else {
		state.c_lflag &= ~ICANON;
		state.c_cc[VMIN] = 1;
		if (tcsetattr(STDIN_FILENO, TCSANOW, &state)) {
			fprintf(stderr, "Cannot set tty state\n");
		}
	}
	if (argc >= 2) {
		if (!strcmp(argv[1], "-h") || !strcmp(argv[1], "--help")) {
			fprintf(stderr, "Usage: %s /dev/hidraw0\n", argv[0]);
			return EXIT_SUCCESS;
		} else {
			smartboard_path = argv[1];
		}
	} else {
		fprintf(stderr, "Usage: %s /dev/hidraw0\n", argv[0]);
		return EXIT_FAILURE;
	}
	/* open uHID and hidraw */
	fprintf(stderr, "Open uhid-cdev %s\n", uhid_path);
	uhid_fd = open(uhid_path, O_RDWR | O_CLOEXEC);
	if (uhid_fd < 0) {
		fprintf(stderr, "Cannot open uhid-cdev %s: %m\n", uhid_path);
		return EXIT_FAILURE;
	}

	fprintf(stderr, "Open hidraw (%s)\n", smartboard_path);
	smartboard_fd = open(smartboard_path, O_RDWR | O_CLOEXEC);
	if (smartboard_fd < 0) {
		fprintf(stderr, "Cannot open hidraw (%s): %m\n", smartboard_path);
		return EXIT_FAILURE;
	}
	/* ------------------- */
	fprintf(stderr, "Create uhid device\n");
	if (uhid_create(uhid_fd)) {
		close(uhid_fd);
		return EXIT_FAILURE;
	}
	pfds[0].fd = smartboard_fd;
	pfds[0].events = POLLIN;
	pfds[1].fd = uhid_fd;
	pfds[1].events = POLLIN;
	pfds[2].fd = STDIN_FILENO;
	pfds[2].events = POLLIN;
	fprintf(stderr, "Press 'q' to quit...\n");
	
	char buff[256];
	int res = ioctl(smartboard_fd, HIDIOCGRAWNAME(256), buff);
	if (res < 0)
		perror("HIDIOCGRAWNAME");
	else
		printf("Raw Name: %s\n", buff);
	
	while (1) {
		if (poll(pfds, NUM_PFDS, -1) < 0) {
			fprintf(stderr, "Cannot poll for fds: %m\n");
			break;
		}
		if (pfds[0].revents & POLLHUP) {
			fprintf(stderr, "Received HUP on stdin\n");
			break;
		}
		if (pfds[1].revents & POLLHUP) {
			fprintf(stderr, "Received HUP on uhid-cdev\n");
			break;
		}
		if (pfds[2].revents & POLLHUP) {
			fprintf(stderr, "Received HUP on stdin\n");
			break;
		}
		if (pfds[0].revents & POLLIN) {
			if (smartboard(uhid_fd, smartboard_fd))
				break;
		}
		if (pfds[1].revents & POLLIN) {
			if (event(uhid_fd))
				break;
		}
		if(pfds[2].revents & POLLIN) {
			if (keyboard())
				break;
		}
	}
	fprintf(stderr, "Destroy uhid device\n");
	uhid_destroy(uhid_fd);
	return EXIT_SUCCESS;
}
