#ifndef S_UHID_H
#define S_UHID_H

#include <linux/uhid.h>

int uhid_write(int fd, const struct uhid_event *ev);

int uhid_create(int fd);

void uhid_destroy(int fd);

int send_key(int fd, unsigned char key);

#endif
