#include "defines.h"

#include <stdio.h>
#include <unistd.h>
#include <stdbool.h>

#include "uhid.h"

extern bool expose_leds;

struct smartboard_model {
	int type_id;
	int report_size;
	int data_offset;
};

struct smartboard_model SB600 = {
	.type_id = 0x01,
	.report_size = 18,
	.data_offset = 0
};

struct smartboard_model SB800 = {
	.type_id = 0x60,
	.report_size = 64,
	.data_offset = 2
};

struct smartboard_model *smartboard_model = &SB600;

static unsigned char compute_checksum(unsigned char *data, int length) {
	int i;
	unsigned char XOR;
	unsigned char c;

	for (XOR = 0, i = 1; i < length; i++) {
		c = (unsigned char)data[i];
		XOR ^= c;
	}
	return XOR;
}

static unsigned char encode_length(int length) {
	unsigned char result;
	result = (unsigned char)length ^ '\x0f';
	result <<= 4;
	result += length;
	return result;
}

static int send_command(int smartboard_fd, unsigned char command, unsigned char *data, int data_length) {
	if(data_length > (smartboard_model->report_size)-4) {
		fprintf(stderr, "error: send_command called with data_length of %d (max is %d-4)\n", data_length, smartboard_model->report_size);

		return -1;
	}

	int length;
	unsigned char report[MAX_REPORT_SIZE] = {0}; //NO SPACE FOR NULL BYTE, don't handle as string
	report[0] = REPORT_ID;
	report[1] = encode_length(data_length);
	report[2] = command;
	int i;
	for (i = 0; i < data_length; i++) {
		report[i+3] = data[i];
	}
	length = data_length + 4; //4 bytes other than data
	report[length-1] = compute_checksum(report, length-1);

	write(smartboard_fd, report, smartboard_model->report_size);
	return length;
}
void switch_leds(int smartboard_fd, int current_tool) {
	unsigned char data[2] = "\xff\x00";
	if(current_tool) {
		data[1] = '\x20'; // 0b00100000
		data[1] = data[1] >> current_tool;
	}
	send_command(smartboard_fd, '\x07', data, 2);
}
void tools_changed(int uhid_fd, int smartboard_fd, unsigned char tools) {

	static int previous_tool;
	int current_tool = 0;

	if (!tools) {
		current_tool = 0;
	}
	if (tools & 0b00010000) { //BLACK
		current_tool = 1;
	}
	if (tools & 0b00001000) { //RED
		current_tool = 2;
	}
	if (tools & 0b00000010) { //GREEN
		current_tool = 4;
	}
	if (tools & 0b00000001) { //BLUE
		current_tool = 5;
	}

	if (tools & 0b00000100) { //ERASER
		current_tool = 3;
	}

	if(current_tool == previous_tool) { //actually, no change has happened
		return;
	}

	switch (current_tool) {
		case 1:
			send_key(uhid_fd, 0x04);
			break;
		case 2:
			send_key(uhid_fd, 0x05);
			break;
		case 3:
			send_key(uhid_fd, 0x06);
			break;
		case 4:
			send_key(uhid_fd, 0x07);
			break;
		case 5:
			send_key(uhid_fd, 0x08);
			break;
	}

	if (!expose_leds) {
		switch_leds(smartboard_fd, current_tool);
	}
	
	previous_tool = current_tool;
}

void buttons_changed(int uhid_fd, int smartboard_fd, unsigned char buttons) {

	int current_button = 0;

	if (!buttons) {
		current_button = 0;
	}
	if (buttons & 0b00000001) { //KEYBOARD
		current_button = 1;
	}
	if (buttons & 0b00000010) { //RIGHT_CLICK
		current_button = 2;
	}
	if (buttons & 0b00000100) { //HELP
		current_button = 3;
	}

	switch (current_button) {
		case 1:
			/*TODO launch (on-screen keyboard) program specified in config*/
			printf("STUB: TODO launch (on-screen keyboard) program specified in config\n");
			break;
		case 2:
			/*TODO send right click event*/
			printf("STUB: TODO send right click event\n");
			break;
		case 3:
			/*TODO launch (help) program specified in config*/;
			printf("STUB: TODO launch (help) program specified in config\n");
			break;
	}
}

int process_command(int uhid_fd, int smartboard_fd, char command, char *data) {
	switch(command) {
		case 0x0: /* CHECK DRIVER PRESENCE */
			send_command(smartboard_fd, command, "\x01", 1);
			break;

		case 0x05: /* PENTRAY STATUS */
			tools_changed(uhid_fd, smartboard_fd, data[0]);
			break;
		case 0x06: /* Buttons */
			buttons_changed(uhid_fd, smartboard_fd, data[0]);
			break;
	}
}
