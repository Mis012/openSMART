#ifndef S_SMARTBOARD_H
#define S_SMARTBOARD_H

struct smartboard_model {
	int type_id;
	int report_size;
	int data_offset;
};
extern struct smartboard_model *smartboard_model;

int process_command(int uhid_fd, int smartboard_fd, char command, char *data);

#endif
