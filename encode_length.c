#include <stdio.h>

static unsigned char encode_lenght(int lenght) {
	unsigned char result;
	result = (unsigned char)lenght ^ '\x0f';
	result <<= 4;
	result += lenght;
	return result;
}

int main(int argc, char **argv) {
	printf("1 > %#02x\n", encode_lenght(1));
	printf("2 > %#02x\n", encode_lenght(2));
	printf("3 > %#02x\n", encode_lenght(3));
}
